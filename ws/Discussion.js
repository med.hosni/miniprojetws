const express = require('express')
const mysql = require('mysql')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------

//GET BY USER_ID
router.get("/showByUser/:idUser", (req, res) => {
    pool.query("SELECT * FROM t_discussion WHERE ID_USER = ?",[req.params.idUser], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//GET BY ID
router.get("/showById/:idDisc", (req, res) => {
    pool.query("SELECT * FROM t_discussion WHERE DISCUSSION_ID = ?",[req.params.idDisc], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------

//POST
//Create Discussion
router.post("/add", (req, res) => {
    pool.query("INSERT INTO t_discussion (ID_USER, ID_OTHER) VALUES (?, ?)", [
        req.body.ID_USER,
        req.body.ID_OTHER
        ], (err, Reserv_rows, fields) => {
            res.status(200)
            res.json(Reserv_rows)
        })
})


//DELETE
//Delete Discussion
router.delete("/delete/:idDisc", (req, res) => {
    pool.query("DELETE FROM t_discussion WHERE DISCUSSION_ID = ?", [
        req.params.idDisc
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})



















module.exports = router;