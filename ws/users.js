const express = require('express')
const mysql = require('mysql')
//const idGenerator = require('../utils/id_generator')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//get user 
router.get("/show", (req, res) => {
    pool.query("SELECT * FROM t_user", (err, user_rows, fields) => {
        res.status(200)
        res.json(user_rows)
    })
})
//add users 
router.post("/addUser", (req, res) => {
  //  console.log(req.param());
    pool.query("INSERT INTO `t_user`(`USER_IMG`,`USER_MAIL`, `USER_NAME`) VALUES (?,?,?)", [
        req.body.USER_IMG,
        req.body.USER_MAIL,
        req.body.USER_NAME
        ], (err, user_rows, fields) => {
            
        res.status(200)
        res.json(user_rows)
    })
   })


router.put("/updateUser", (req, res) => {
    pool.query(
        "UPDATE `t_user` SET `USER_AGE`=?,`USER_CAPTAIN`=?,`USER_DISPO`=?,`USER_MAIL`=?,`USER_NAME`=?,`USER_PHONE`=?,`USER_SECTEUR`=?,`USER_SOLO`=?,`USER_SURNAME`=?,", [
        req.body.USER_AGE,
        req.body.USER_CAPTAIN,
        req.body.USER_DISPO,
        req.body.USER_MAIL,
        req.body.USER_NAME,
        req.body.USER_PHONE,
        req.body.USER_SECTEUR,
        req.body.USER_SOLO,
        req.body.USER_SOLO,
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})












module.exports = router;