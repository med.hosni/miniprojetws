const express = require('express')
const mysql = require('mysql')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------

//GET BY PUBLICATION_ID
router.get("/showByPub/:idPub", (req, res) => {
    pool.query("SELECT * FROM t_image WHERE IMAGE_PUB_ID = ?",[req.params.idPub], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//GET BY ID
router.get("/showById/:idImg", (req, res) => {
    pool.query("SELECT * FROM t_image WHERE IMAGE_ID = ?",[req.params.idImg], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------


//POST
//Create Image
router.post("/add", (req, res) => {
    pool.query("INSERT INTO t_image (IMAGE_DATE, IMAGE_URL, IMAGE_PUB_ID) VALUES (?, ?, ?)", [
        req.body.IMAGE_DATE,
        req.body.IMAGE_URL,
        req.body.IMAGE_PUB_ID,
        ], (err, Reserv_rows, fields) => {
            res.status(200)
            res.json(Reserv_rows)
        })
})

//PUT
//Update Image
router.put("/update/:idImg", (req, res) => {
    pool.query("UPDATE t_image SET `IMAGE_DATE` = ?,`IMAGE_URL`=? WHERE IMAGE_ID = ?", [
        req.body.IMAGE_DATE,
        req.body.IMAGE_URL,
        req.params.idImg
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})


//DELETE
//Delete Image
router.delete("/delete/:idImg", (req, res) => {
    pool.query("DELETE FROM t_image WHERE IMAGE_ID = ?", [
        req.params.idImg
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})



module.exports = router;