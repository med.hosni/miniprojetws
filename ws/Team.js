const express = require('express')
const mysql = require('mysql')
//const idGenerator = require('../utils/id_generator')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//get Teams 
router.get("/showTeams", (req, res) => {
    pool.query("SELECT * FROM `t_team`", (err, user_rows, fields) => {
        res.status(200)
        res.json(user_rows)
    })
})

//get Teams 
router.get("/showTeamsByName", (req, res) => {
    pool.query("SELECT * FROM `t_team` WHERE TEAM_NAME =?", (err, user_rows, fields) => {
        res.status(200)
        res.json(user_rows)
    })
})
//add Team
router.post("/addTeam", (req, res) => {
  //  console.log(req.param());
    pool.query("INSERT INTO `t_team`(`TEAM_NB_PLAYERS`, `TEAM_NAME`, `ID_CAPTAIN`) VALUES (?,?,?)", [
        req.body.TEAM_NB_PLAYERS,
        req.body.TEAM_NAME,
        req.body.ID_CAPTAIN
        ], (err, user_rows, fields) => {
            
        res.status(200)
        res.json(user_rows)
    })
   })


router.put("/updateUser", (req, res) => {
    pool.query(
        "", [
        req.body.USER_AGE,
        req.body.USER_CAPTAIN,
        req.body.USER_DISPO,
        req.body.USER_MAIL,
        req.body.USER_NAME,
        req.body.USER_PHONE,
        req.body.USER_SECTEUR,
        req.body.USER_SOLO,
        req.body.USER_SOLO,
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})












module.exports = router;