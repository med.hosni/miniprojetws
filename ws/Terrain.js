const express = require('express')
const mysql = require('mysql')
var multer = require('multer');
var uuidv4 = require('uuid/v4')

var storage = multer.diskStorage({
    destination: function(req,file,cb){
        cb(null,'public/stadiums/');
    },
    filename: function(req,file,cb){
        cb(null,uuidv4() + '.png');
    }

})



var upload = multer({storage: storage})
//const idGenerator = require('../utils/id_generator')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------


//GET_ALL
router.get("/showAll", (req, res) => {
    pool.query("SELECT * FROM t_stadium", (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})



//GET_Stadium by prix-Interval
router.get("/byPrice/:min/:max", (req, res) => {
    pool.query("SELECT * FROM `t_stadium` WHERE STADIUM_PRICE BETWEEN ? and ?", 
	[req.params.min,
	req.params.max], 
	(err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

/*
//GET_Stadium by Secteur
router.get("/bySecteur/:secteur", (req, res) => {
    pool.query("SELECT * FROM `t_stadium` WHERE STADIUM_SECTEUR LIKE '?'", 
	[req.params.secteur], 
	(err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

*/
//GET_Stadium byTag
router.get("/byTag/:nom", (req, res) => {
    pool.query("SELECT * FROM `t_stadium` WHERE STADIUM_NAME = ?",
    [req.params.nom],
    (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

/*
//GET_Stadium byType
router.get("/byType/:type", (req, res) => {
    pool.query("SELECT * FROM `t_stadium` WHERE STADIUM_TYPE LIKE '?'", 
	[req.params.type], 
	(err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

*/
//GET_Stadium by ID_Stadium
router.get("/showById/:idStadium", (req, res) => {
    pool.query("SELECT * FROM T_STADIUM WHERE STADIUM_ID = ?", [req.params.idStadium], (err, rows, fields) => {
        res.status(200)
        res.json(rows[0])
    })
})

//GET_Stadium MostPopular
router.get("/mostPopular", (req, res) => {
    pool.query("SELECT * FROM T_STADIUM ORDER BY NB_VUES_STADIUM DESC", 
	(err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//GET_Stadium MostRated
router.get("/mostRated", (req, res) => {
    pool.query("SELECT * FROM T_STADIUM ORDER BY STADIUM_GLOBAL_RATING DESC", 
	(err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})





//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//POST
//Create Terrain
router.post("/add",upload.any(), (req, res) => {

    let filename = req.files[0].filename; 



    pool.query("INSERT INTO T_STADIUM(STADIUM_LATITUDE, STADIUM_LONGITUDE, STADIUM_NAME, STADIUM_NB_PERSONS,STADIUM_PHONE,STADIUM_PRICE,STADIUM_AMBIANCE_RATING,STADIUM_EQUIPPEMENT_RATING,STADIUM_GLOBAL_RATING,STADIUM_QUALITE_RATING,STADIUM_SERVICE_RATING,STADIUM_SECTEUR,STADIUM_ADRESSE,STADIUM_TYPE, NB_VUES_STADIUM, STADIUM_IMAGE) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
        
        req.body.STADIUM_LATITUDE,
        req.body.STADIUM_LONGITUDE,
        req.body.STADIUM_NAME,
        req.body.STADIUM_NB_PERSONS,
		req.body.STADIUM_PHONE,
        req.body.STADIUM_PRICE,
        req.body.STADIUM_AMBIANCE_RATING,
        req.body.STADIUM_EQUIPPEMENT_RATING,
		req.body.STADIUM_GLOBAL_RATING,
		req.body.STADIUM_QUALITE_RATING,
		req.body.STADIUM_SERVICE_RATING,
        req.body.STADIUM_SECTEUR,
        req.body.STADIUM_ADRESSE,
        req.body.STADIUM_TYPE,
        req.body.NB_VUES_STADIUM,
        filename
        ], (err, rows, fields) => {
            if(err){
                console.log(err);
                
            }
            res.status(200)
        })
})



//PUT
//Update Terrain (Coordonnées)
router.put("/Update/:idTerrain", (req, res) => {
    pool.query("UPDATE `T_STADIUM` SET `STADIUM_LATITUDE`=?,`STADIUM_LONGITUDE`=?,`STADIUM_NAME`=?,`STADIUM_NB_PERSONS`=?,`STADIUM_PHONE`=?,`STADIUM_PRICE`=? , `STADIUM_SECTEUR`=?,`STADIUM_TYPE`=? WHERE STADIUM_ID=?", [
        req.body.STADIUM_LATITUDE,
        req.body.STADIUM_LONGITUDE,
        req.body.STADIUM_NAME,
        req.body.STADIUM_NB_PERSONS,
		req.body.STADIUM_PHONE,
        req.body.STADIUM_PRICE,
		req.body.STADIUM_SECTEUR,
        req.body.STADIUM_TYPE,
		req.params.idTerrain
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
	
})

//--------------------------------------------------------------------------------------------------------------
//---------------------------------------------RATINGS Update---------------------------------------------
//--------------------------------------------------------------------------------------------------------------


//---------------------------------------------Rating Global Update---------------------------------------------
//PUT
//Update Terrain (ratingGlobal)
router.put("/ratingGlobal/:idTerrain", (req, res) => {
    pool.query("UPDATE T_STADIUM SET STADIUM_GLOBAL_RATING=?  WHERE STADIUM_ID = ?", [
        req.body.STADIUM_GLOBAL_RATING,
		req.params.idTerrain
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
	
})

//---------------------------------------------Rating Service Update-----------------------------------------------------------------
//PUT
//Update Terrain (ratingService)
router.put("/ratingService/:idTerrain", (req, res) => {
    pool.query("UPDATE T_STADIUM SET STADIUM_SERVICE_RATING=?  WHERE STADIUM_ID = ?", [
        req.body.STADIUM_SERVICE_RATING,
		req.params.idTerrain
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
	
})



//---------------------------------------------Rating Qualité Update-----------------------------------------------------------------
//PUT
//Update Terrain (ratingQualite)
router.put("/ratingQualite/:idTerrain", (req, res) => {
    pool.query("UPDATE T_STADIUM SET STADIUM_QUALITE_RATING=?  WHERE STADIUM_ID = ?", [
        req.body.STADIUM_QUALITE_RATING,
		req.params.idTerrain
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
	
})
//---------------------------------------------Rating Equippements Update-----------------------------------------------------------------
//PUT
//Update Terrain (ratingEquip)
router.put("/ratingEquip/:idTerrain", (req, res) => {
    pool.query("UPDATE T_STADIUM SET STADIUM_EQUIPPEMENT_RATING=?  WHERE STADIUM_ID = ?", [
        req.body.STADIUM_EQUIPPEMENT_RATING,
		req.params.idTerrain
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
	
})

//---------------------------------------------Rating Ambiance Update-----------------------------------------------------------------
//PUT
//Update Terrain (ratingAmbiance)
router.put("/ratingAmbiance/:idTerrain", (req, res) => {
    pool.query("UPDATE T_STADIUM SET STADIUM_AMBIANCE_RATING=?  WHERE STADIUM_ID = ?", [
        req.body.STADIUM_AMBIANCE_RATING,
		req.params.idTerrain
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
	
})

//--------------------------------------------------------------------------------------------------------------

module.exports = router;