const express = require('express')
const mysql = require('mysql')
//const idGenerator = require('../utils/id_generator')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------


//GET_ALL
router.get("/showAll", (req, res) => {
    pool.query("SELECT * FROM t_reservation", (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})


//GET_Reservation by UserId
router.get("/showByUser/:idUser", (req, res) => {
    pool.query("SELECT * FROM t_reservation WHERE ID_USER= ?", 
    [req.params.idUser], (err, rows, fields) => {
        res.status(200)
        res.json(rows[0])
    })
})



//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------

//POST
//Create Reservation
router.post("/add", (req, res) => {
    pool.query("INSERT INTO t_reservation (RESERVATION_DATE, RESERVATION_HEURE, ID_RIVAL, ID_STADIUM_RESERV, ID_USER) VALUES (?, ?, ?, ?, ?)", [
        req.body.RESERVATION_DATE,
        req.body.RESERVATION_HEURE,
        req.body.ID_RIVAL,
        req.body.ID_STADIUM_RESERV,
		req.body.ID_USER
        ], (err, Reserv_rows, fields) => {
            res.status(200)
            res.json(Reserv_rows)
        })
})



//PUT
//Update Reservation
router.put("/update/:idReserv", (req, res) => {
    pool.query("UPDATE t_reservation SET `RESERVATION_DATE`=?,`RESERVATION_HEURE`=?,`ID_RIVAL`=?,`ID_STADIUM_RESERV`=? WHERE RESERVATION_ID = ?", [
        req.body.RESERVATION_DATE,
        req.body.RESERVATION_HEURE,
        req.body.ID_RIVAL,
        req.body.ID_STADIUM_RESERV,
        req.params.idReserv
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})



//DELETE
//Delete Reservation
router.delete("/delete/:idReserv", (req, res) => {
    pool.query("DELETE FROM t_reservation WHERE RESERVATION_ID = ?", [
        req.params.idReserv
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})



module.exports = router;