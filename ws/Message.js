const express = require('express')
const mysql = require('mysql')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------

//GET BY Discussion_ID
router.get("/showByDisc/:idDisc", (req, res) => {
    pool.query("SELECT * FROM t_message WHERE ID_MSG_DISCUSS = ?",[req.params.idDisc], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//GET BY ID
router.get("/showById/:idMsg", (req, res) => {
    pool.query("SELECT * FROM t_message WHERE MESSAGE_ID = ?",[req.params.idMsg], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------

//POST
//Create Message
router.post("/add", (req, res) => {
    pool.query("INSERT INTO t_message (MESSAGE_CONTENU, MESSAGE_DATE, ID_MSG_USER, ID_MSG_DISCUSS) VALUES (?, ?, ?, ?)", [
        req.body.MESSAGE_CONTENU,
        req.body.MESSAGE_DATE,
        req.body.ID_MSG_USER,
        req.body.ID_MSG_DISCUSS
        ], (err, Reserv_rows, fields) => {
            res.status(200)
            res.json(Reserv_rows)
        })
})


module.exports = router;