const express = require('express')
const mysql = require('mysql')

const router = express.Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    database: "whistle"
})

function getConnection(){
    return pool
}

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------


//GET BY PUBLICATION_ID
router.get("/showByPub/:idPub", (req, res) => {
    pool.query("SELECT * FROM t_commentaire c, t_user u WHERE c.COMMENT_USER_ID = u.USER_ID and COMMENT_ID_PUB = ?",[req.params.idPub], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//GET BY ID
router.get("/showById/:idComment", (req, res) => {
    pool.query("SELECT * FROM t_commentaire WHERE COMMENT_ID = ?",[req.params.idComment], (err, rows, fields) => {
        res.status(200)
        res.json(rows)
    })
})

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------


//POST
//Create Comment
router.post("/add", (req, res) => {
    pool.query("INSERT INTO t_commentaire (COMMENT_CONTENU, COMMENT_DATE, COMMENT_USER_ID, COMMENT_ID_PUB) VALUES (?, ?, ?, ?)", [
        req.body.COMMENT_CONTENU,
        req.body.COMMENT_DATE,
        req.body.COMMENT_USER_ID,
        req.body.COMMENT_ID_PUB
        ], (err, Reserv_rows, fields) => {
            res.status(200)
            res.json(Reserv_rows)
        })
})

//PUT
//Update Comment
router.put("/update/:idComment", (req, res) => {
    pool.query("UPDATE t_commentaire SET `COMMENT_DATE` = ?,`COMMENT_CONTENU`=? WHERE COMMENT_ID = ?", [
        req.body.COMMENT_DATE,
        req.body.COMMENT_CONTENU,
        req.params.idComment
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})


//DELETE
//Delete Comment
router.delete("/delete/:idComment", (req, res) => {
    pool.query("DELETE FROM t_commentaire WHERE COMMENT_ID = ?", [
        req.params.idComment
    ], (err, rows, fields) => {
        res.status(204)
        res.end();
    })
})





module.exports = router;